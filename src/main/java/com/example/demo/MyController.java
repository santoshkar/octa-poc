package com.example.demo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.okta.sdk.resource.user.User;

@RestController
public class MyController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/")
	public String showMsg(Authentication authentication) {
		System.out.println(authentication.getName());
		authentication.getAuthorities().forEach(myvar -> {
			System.out.println(myvar.getAuthority());
		});
		System.out.println(restTemplate);
		return "Welcome, you are logged in";
	}

	@GetMapping("/users")
	public List<User> getUsers() {
		// String theUrl = "https://dev-723438.okta.com/api/v1/users?limit=25";
		String theUrl = "https://dev-723438.okta.com/api/v1/users";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = createHttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> response = restTemplate.exchange(theUrl, HttpMethod.GET, entity, String.class);
		System.out.println("Result - status (" + response.getStatusCode() + ") has body: " + response.hasBody());

		Gson gson = new Gson();
		Type founderListType = new TypeToken<ArrayList<User>>() {
		}.getType();
		List<User> userList = gson.fromJson(response.getBody(), founderListType);

		return userList;
	}

	private HttpHeaders createHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "SSWS 00YVeqxhEAtskYciY7dKCYR0FTz0TJ-pLoV6UXEUt7");
		return headers;
	}

}
