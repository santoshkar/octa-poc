package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import com.okta.sdk.resource.user.User;

public class OctaUserList {
    private List<User> users;
 
    public OctaUserList() {
    	users = new ArrayList<>();
    }
 
    public void setUsers(List<User> users) {
		this.users = users;
	}
    
    public List<User> getUsers() {
		return users;
	}

	@Override
	public String toString() {
		return "OctaUserList [users=" + users + "]";
	}
    
    
}